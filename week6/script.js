// task1 hw6
function calendar(){
    let calendar = document.getElementById('calendar');
    let weekdays = document.createElement('div');
    // create header

    let header = document.createElement('div');
    header.className = 'header';
    let header_year = document.createElement('span');
    let line = document.createElement('span');
    let header_month = document.createElement('span');
    header_year.innerHTML = '2016';
    header_month.innerHTML = 'მარტი';
    line.innerHTML = '|';
    header.appendChild(header_year);
    header.appendChild(line);
    header.appendChild(header_month);
    weekdays.appendChild(header);

    weekdays.className = 'weekdays';
    let daysArr = ['კვ', 'ორ', 'სამ', 'ოთხ', 'ხუთ', 'პარ', 'შაბ'];
    for(let i = 0; i < daysArr.length; i++){
        let day = document.createElement('span');
        day.innerHTML = daysArr[i];
        weekdays.appendChild(day);
    }
    // create days
    let days = document.createElement('div');
    days.className = 'days';
    for(var i = 1; i <= 31; i++){
        var day = document.createElement('span');
        day.innerHTML = i;
        days.appendChild(day);
        if(i==7 || i==14 || i==21 || i==28){
            days.className = 'days row';
        }
        if(i == 28){
            day.className = 'green';
        }
    }
    weekdays.appendChild(days);
        calendar.appendChild(weekdays);
}

// calendar();

//task 2

function task2(){
    let div = document.getElementsByTagName('div');
    div[0].style.height = '200px';

    document.getElementById('parent').addEventListener('click', function(){
        document.body.children[0].style.backgroundColor = 'red';
        
    });

    document.getElementById('firstChild').addEventListener('click', function(){
        document.body.children[0].childNodes[1].style.backgroundColor = 'red';
    });

    document.getElementById('nextSib').addEventListener('click', function(){
        document.body.children[0].childNodes[2].nextElementSibling.style.backgroundColor = 'red';
    })  

}
task2();