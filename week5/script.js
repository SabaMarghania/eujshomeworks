// pirveli davaleba

function test1(){
    let box = document.getElementById("box");
    box.innerHTML = "ორშაბათი";
}
function test2(){
    let box = document.getElementById("box");
    box.innerHTML = "სამშაბათი";
}
function test3(){
    let box = document.getElementById("box");
    box.innerHTML = "ოთხშაბათი";
}
function test4(){
    let box = document.getElementById("box");
    box.innerHTML = "ხუთშაბათი";
}
function test5(){
    let box = document.getElementById("box");
    box.innerHTML = "პარასკევი";    
}
function test6(){
    let box = document.getElementById("box");
    box.innerHTML = "შაბათი";    
}
function test7(){
    let box = document.getElementById("box");
    box.innerHTML = "კვირა";     
}
// ------

function task2(){
    let num1 = document.getElementById("num1").value;
    let operator = document.getElementById("operator").value;
    let num2 = document.getElementById("num2").value;
    let result = 0;
    switch(operator){
        case "+":
            result = parseInt(num1) + parseInt(num2);
            break;
        case "-":
            result = (num1 - num2);
            break;
        case "*":
            result = (num1 * num2);
            break;
        case "/":
            result = (num1 / num2);
            break;
        case "^":
            result = (Math.pow(num1, num2));
            break;
         case "sqrt":
            result = (Math.sqrt(num1));
            break;

    }
    document.getElementById('result').innerHTML = result;
    
}
// task2();

function task5(){
    const random = document.getElementById("random");
    let randomNumber = Math.floor(Math.random() * 30);
    random.innerHTML = randomNumber;

}
function task6(){
    const random2 = document.getElementById("random2");
    const input1 = document.getElementById("input1").value;
    const input2 = document.getElementById("input2").value;
    let randomNumber2 = Math.floor(Math.random() * (input2 - input1 + 1)) + input1;
    random2.innerHTML = randomNumber2;


}

function task7(){
      
    let tableChange = document.getElementById('tableChange')
    let sveti = document.getElementById('sveti').value
    let striqoni = document.getElementById('striqoni').value
    let suratebi = document.getElementById('suratebi').value
    let tableBody = document.createElement("tbody");
    // rows 
    let imgs3 = ["./images/1.jpg","./images/2.jpg","./images/3.jpeg","./images/4.jpg","./images/5.jpeg","./images/6.jpg","./images/7.jpeg","./images/8.jpeg","./images/9.jpeg","./images/10.jpeg"]

    for (let j = 0; j < sveti; j++) {
      //  სვეთი
      let row = document.createElement("tr");
  
      for (let i = 0; i < striqoni; i++) {

        let td = document.createElement("td");
        let tdImg = document.createElement("img");
        tdImg.style.width='100px'
        tdImg.style.height='100px'
        tdImg.src=imgs3[Math.floor(Math.random() * suratebi)]
        td.appendChild(tdImg);

        row.appendChild(td);
      }
  
      //row added to table body
      tableBody.appendChild(row);
    }
  
   // adding tbody to table
    tableChange.appendChild(tableBody);
}