function task1(){
    let box = document.getElementById('box');
    let down = document.getElementById('btn-down');
    let up = document.getElementById('btn-up');
    let left = document.getElementById('btn-left');
    let right = document.getElementById('btn-right');
    let downLeft = document.getElementById('btn-downLeft');
    let downRight = document.getElementById('btn-downRight');
    let upLeft = document.getElementById('btn-upLeft');
    let upRight = document.getElementById('btn-upRight');

    down.addEventListener('click', function(){
        box.style.marginTop = box.offsetTop + 10 + 'px';
    }
    );
    up.addEventListener('click', function(){
        // box.style.marginTop = '0'
        box.style.marginTop = box.offsetTop - 30 + 'px';
    }
    );
    left.addEventListener('click', function(){
        box.style.marginLeft = box.offsetLeft - 20 + 'px';
    }
    );
    right.addEventListener('click', function(){
        box.style.marginLeft = box.offsetLeft + 10 + 'px';
    }
    );
    downLeft.addEventListener('click', function(){
        box.style.marginTop = box.offsetTop + 10 + 'px';
        box.style.marginLeft = box.offsetLeft - 20 + 'px';
    }
    );
    downRight.addEventListener('click', function(){
        box.style.marginTop = box.offsetTop + 10 + 'px';
        box.style.marginLeft = box.offsetLeft + 20 + 'px';
    }
    );
    upLeft.addEventListener('click', function(){
        box.style.marginTop = box.offsetTop - 30 + 'px';
        box.style.marginLeft = box.offsetLeft - 30 + 'px';
    }
    );
    upRight.addEventListener('click', function(){
        box.style.marginTop = box.offsetTop - 30 + 'px';
        box.style.marginLeft = box.offsetLeft + 20 + 'px';
    }
    );
}
task1();

function task2(){
    let box = document.getElementById('box2');

    document.addEventListener('keydown', function(event){
        if(event.keyCode == 39){
            box.style.width = box.offsetWidth + 10 + 'px';
        box.style.backgroundColor = 'green';
    }
        if(event.keyCode == 37){
            box.style.width = box.offsetWidth - 10 + 'px';
            box.style.backgroundColor = 'red';
    }
        if(event.keyCode == 38){
            box.style.height = box.offsetHeight - 10 + 'px';
            document.body.style.overflow = 'hidden';
            box.style.backgroundColor = 'green';
    }else{
             document.body.style.overflow = 'visible';
    }
            if(event.keyCode == 40){
                box.style.height = box.offsetHeight + 10 + 'px';
                box.style.backgroundColor = 'red';
        }
        
    }
    );


}
task2();





