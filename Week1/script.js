// task 1
function func(text){
    document.write("<h1>task 1</h1>")
    document.write("<u><i><b>"+text+"</b></i></u>"+"<br>")
}
func("hello")

//task 2
function add(a,b){
    document.write("<h1>task 2</h1>")

    return a+b;
}
document.write(add(5,10))

//task 3
function text(text,font){
    document.write("<h1>task 3</h1>")

    return font + text + font
}
document.write(text("<h1>","hello","</h1>"))

//task 4

function table(width){
    let table = document.getElementById('table')
    table.style.width = width

}
table("500px")

function table2(height){
    let table2 = document.getElementById('table2')
    table2.style.height = height

}
table2("150px")

function table3(width,height){
    let table3 = document.getElementById('table3')
    table3.style.width = width
    table3.style.height = height

}
table3("550px",'200px')

function table4(background){
    let table4 = document.getElementById('table4')
    table4.style.backgroundColor = background

}
table4("red")

function table5(borderSize){
    let table5 = document.getElementById('table5')
    table5.style.border = borderSize

}
table5("10px solid black")

function table6(width,height,background,borderSize){
    let table6 = document.getElementById('table6')
    table6.style.width = width
    table6.style.height = height
    table6.style.background = background
    table6.style.border = borderSize

}
table6("300px","300px","green","5px solid red")

function table7(width,height,background,borderSize,fontsize,color,font,boldness){
    let table7 = document.getElementById('table7')
    table7.style.width = width
    table7.style.height = height
    table7.style.background = background
    table7.style.border = borderSize
    table7.style.fontSize = fontsize
    table7.style.color = color
    table7.style.fontStyle = font
    table7.style.fontWeight = boldness

}
table7("400px","200px","purple","8px solid gray","25px","white","italic","700")

//task 11
function nums(){
    document.write("<br><br><h1 style='color:red'> task 11</h1>")

    for(let i=0;i<10;i++){
    document.write("<h3>"+i+"</h3>")

    }
}
nums()

//task 12
function nums2(n){
    document.write("<br><br><h1 style='color:red'> task 12</h1>")

    for(let i=1;i<n;i++){
    document.write("<h3>"+i+"</h3>")

    }
}
nums2(13)

//task 13
function nums3(m,n){
    document.write("<br><br><h1 style='color:red'> task 13</h1>")

    for(let i=m;i<n;i++){
    document.write("<h3>"+i+"</h3>")

    }
}
nums3(1,15)


//task 14

function nums4(m,n){
    document.write("<br><br><h1 style='color:red'> task 14</h1>")
    if(m>n){
        for(let i=m;i>n;i--){
            document.write("<h3>"+i+"</h3>")
        
        }
    }else{
        for(let i=m;i<n;i++){
            document.write("<h3>"+i+"</h3>")
        
        }
    }

}
nums4(25,15)

//task 15
function changeColAndRow(rows,cols){
    let tableChange = document.getElementById('tableChange')
    var tableBody = document.createElement("tbody");
  
    // rows 
    for (var j = 0; j < rows; j++) {
      //  სვეთი
      var row = document.createElement("tr");
  
      for (var i = 0; i < cols; i++) {
        // create element <td> and text  
        var td = document.createElement("td");
        var tdText = document.createTextNode("BOX");
  
        td.appendChild(tdText);
        row.appendChild(td);
      }
  
      //row added to table body
      tableBody.appendChild(row);
    }
  
   // adding tbody to table
    tableChange.appendChild(tableBody);
  
}
changeColAndRow(5,3)



function changeColAndRow2(rows2,cols2){
    let tableChange2 = document.getElementById('tableChange2')
    var tableBody2 = document.createElement("tbody2");
  
    // rows 
    for (var j_2 = 0; j_2 < rows2; j_2++) {
      //  სვეთი
      var row2 = document.createElement("tr");
  
      for (var i_2 = 0; i_2 < cols2; i_2++) {
        // create element <td> and text  
        var td2 = document.createElement("td");
        var tdText2 = document.createTextNode("სტრიქონი " + cols2+ " სვეტი " + " " + rows2);
  
        td2.appendChild(tdText2);
        row2.appendChild(td2);
      }
  
      //row added to table body
      tableBody2.appendChild(row2);
    }
  
   // adding tbody to table
    tableChange2.appendChild(tableBody2);
  
}
changeColAndRow2(3,4)

function changeColAndRow3(rows3,cols3,width,height,background,borderSize){
    let tableChange3 = document.getElementById('tableChange3')
    var tableBody3 = document.createElement("tbody3");
  
    // rows 
    for (var j_3 = 0; j_3 < rows3; j_3++) {
      //  სვეთი
      var row3 = document.createElement("tr");
  
      for (var i_3 = 0; i_3 < cols3; i_3++) {
        // create element <td> and text  
        var td3 = document.createElement("td");
        var tdText3 = document.createTextNode("სტრიქონი " + cols3+ " სვეტი " + " " + rows3);
  
        td3.appendChild(tdText3);
        row3.appendChild(td3);
      }
  
      //row added to table body
      tableBody3.appendChild(row3);
    }
  
   // adding tbody to table
    tableChange3.appendChild(tableBody3);
  

    //table styles

    tableChange3.style.width = width
    tableChange3.style.height = height
    tableChange3.style.background = background
    tableChange3.style.border = borderSize
}
changeColAndRow3(4,4,'200px','300px',"limegreen",'5px solid black')
